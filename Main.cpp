#include <iostream>


class Stack
{
public:
	Stack()
	{
		std::cout << "Enter the stack size: ";
		std::cin >> stackSize;
		array = new int[stackSize];
	}
	~Stack() {}

	void Push(int a)
	{
		if (lastIndex < stackSize)
		{
			array[++lastIndex] = a;
		}
		else
		{
			std::cout << "Stack is full\n";
		}
	}

	void Pop()
	{
		if (lastIndex > -1)
		{
			std::cout << array[lastIndex] << std::endl;
			array[lastIndex--] = 0;
		}
		else
		{
			std::cout << "Stack is empty!" << std::endl;
		}
	}

	void ShowArray()
	{
		if (lastIndex > -1)
		{
			for (int i = 0; i <= lastIndex; i++)
			{
				std::cout << "[" << i << "] = " << array[i] << std::endl;
			}
		}
		else
		{
			std::cout << "Stack is empty!" << std::endl;
		}
	}

	void FillRandom()
	{
		for (int i = 0; i < stackSize; i++)
		{
			lastIndex = i;
			array[i] = rand();

		}
	}

private:
	int stackSize;
	int lastIndex = -1;
	int* array;
};



int main()
{
	Stack s;
	s.FillRandom();
	s.ShowArray();
	s.Pop();
	s.Pop();
	s.Pop();
	s.ShowArray();

	return 0;
}